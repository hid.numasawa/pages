# 末尾の/がなければ付与する

```java
if (!output.endWith("/")) {
    output += "/";
}
// おすすめはしない
File outputFile = new File(output + filename);
```

# 末尾に/の有無を意識しない

```java
// 推奨
File outputFile = new File(output, filename);
```
